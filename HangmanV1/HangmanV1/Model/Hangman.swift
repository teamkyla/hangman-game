//
//  Hangman.swift
//  HangmanV1
//
//  Created by Kyla Wilson on 4/5/19.
//  Copyright © 2019 Kyla Wilson. All rights reserved.
//

import Foundation

struct Hangman {
    private var phrases: [String]!
    private var hints: [String]!
    
    var tries: Int!
    var hiddenPhrase: String!
    var hint: String!
    
    init() {
        self.phrases = ["Kyla loves Jesus", "To cool for school", "I love to program"]
        self.hints = ["Someone Kyla really loves", "I hate school (not really)", "Coding is fun!"]
        ResetGame()
    }
    
    mutating func DeductTries() {
        if tries > 0 {
            tries -= 1
        }
    }
    
    mutating func ResetGame() {
        self.tries = 5
        
        if let index = phrases.RandomIndex() {
            hiddenPhrase = phrases[index]
            hint = hints[index]
        }
    }
}
