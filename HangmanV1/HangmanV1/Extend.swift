//
//  Extend.swift
//  HangmanV1
//
//  Created by Kyla Wilson on 4/5/19.
//  Copyright © 2019 Kyla Wilson. All rights reserved.
//

import Foundation

extension Array {
    func RandomIndex() -> Int? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return index
    }
}
