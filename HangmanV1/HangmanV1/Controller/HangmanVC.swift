//
//  ViewController.swift
//  HangmanV1
//
//  Created by Kyla Wilson on 4/5/19.
//  Copyright © 2019 Kyla Wilson. All rights reserved.
//

import UIKit

class HangmanVC: UIViewController {
    
    @IBOutlet weak var triesLbl: UILabel!
    @IBOutlet weak var resultLbl: UILabel!
    
    @IBOutlet weak var playAgainBtn: UIButton!
    
    @IBOutlet weak var phraseTF: UITextField!

    var hangman: Hangman!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hangman = Hangman()
        playAgainBtn.isEnabled = false
        playAgainBtn.isHidden = true
        triesLbl.text = "Tries left: \(hangman.tries!)"
        CreateTextfieldButton()
    }

    func CreateTextfieldButton() {
        let doneBtn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
        
        doneBtn.setTitle("Done", for: UIControl.State.normal)
        doneBtn.addTarget(self, action: #selector(HangmanVC.CheckAnswer), for: UIControl.Event.touchUpInside)
        doneBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2608429715)
        
        phraseTF.inputAccessoryView = doneBtn
    }

    @objc func CheckAnswer() {
        if let guess = phraseTF.text {
            if hangman.tries > 0 {
                if hangman.hiddenPhrase.lowercased() == guess.lowercased() {
                    resultLbl.text = "Congratulations, you guessed correctly!"
                    hangman.ResetGame()
                } else {
                    resultLbl.text = "Incorrect guess, try again"
                }
            } else {
                resultLbl.text = "GAME OVER!"
                playAgainBtn.isEnabled = true
                playAgainBtn.isHidden = false
            }
        }
        
        hangman.DeductTries()
        triesLbl.text = "Tries left: \(hangman.tries!)"
        view.endEditing(true)
    }
    
    @IBAction func HintBtnPresed(_ sender: Any) {
        resultLbl.text = hangman.hint
    }

    
    @IBAction func PlayAgainPressed(_ sender: Any) {
        hangman.ResetGame()
        triesLbl.text = "Tries left: \(hangman.tries!)"
        resultLbl.text = "Hangman"
        playAgainBtn.isEnabled = false
        playAgainBtn.isHidden = true
    }
}


