//
//  CircleButton.swift
//  HangmanV1
//
//  Created by Kyla Wilson on 4/5/19.
//  Copyright © 2019 Kyla Wilson. All rights reserved.
//

import UIKit

@IBDesignable
class CircleButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        CustomizeButton()
    }
    
    override func prepareForInterfaceBuilder() {
        CustomizeButton()
    }
    
    func CustomizeButton() {
        self.layer.cornerRadius = 50.0
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
    }

}
