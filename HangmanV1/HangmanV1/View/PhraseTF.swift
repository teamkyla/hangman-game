//
//  PhraseTF.swift
//  HangmanV1
//
//  Created by Kyla Wilson on 4/5/19.
//  Copyright © 2019 Kyla Wilson. All rights reserved.
//

import UIKit

@IBDesignable
class PhraseTF: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        CustomizeTextField()
    }
    
    override func prepareForInterfaceBuilder() {
        CustomizeTextField()
    }
    
    func CustomizeTextField() {
        self.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.3)
        self.layer.cornerRadius = 5.0
        self.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.textAlignment = NSTextAlignment.center
        
        if let p = placeholder {
            let placeHolder = NSAttributedString(string: p, attributes: [.foregroundColor: UIColor.white])
            self.attributedPlaceholder = placeHolder
            self.textColor = UIColor.white
        }
    }

}
